import nem from 'nem-sdk';

import { numberFormatter } from './formatters';

export function isIncome(currentAddress, recipient) {
  return currentAddress === recipient;
}

export function formatAmount(amount) {
  return numberFormatter.format(amount);
}

export function formatAmountRaw(amount) {
  return numberFormatter.format(amount / 1000000);
}

export function timeStampToDate(timeStamp) {
  return new Date(nem.utils.format.nemDate(timeStamp));
}
